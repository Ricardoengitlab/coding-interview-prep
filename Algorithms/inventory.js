function updateInventory(arr1, arr2) {
    let arr3 = []
    arr1 = arr1.concat(arr2)
    arr3.push(arr1[0])
        for(var i = 1; i < arr1.length; i++){
                for(var j = 0; j < arr3.length; j++){
                    if(arr1[i][1] == arr3[j][1]){

                        arr3[j][0] = arr3[j][0] + arr1[i][0]
                        break
                    }
                    if(j == arr3.length - 1){
                        arr3.push(arr1[i])
                        break
                    }
                
            }
        }
    
    // All inventory must be accounted for or you're fired!
    return arr3.sort((a,b) => a[1].toUpperCase().localeCompare(b[1].toUpperCase()));

}

// Example inventory lists
var curInv = [
    [21, "Bowling Ball"],
    [2, "Dirty Sock"],
    [1, "Hair Pin"],
    [5, "Microphone"]
];

var newInv = [
    [2, "Hair Pin"],
    [3, "Half-Eaten Apple"],
    [67, "Bowling Ball"],
    [7, "Toothpaste"]
];

updateInventory(curInv, newInv)